### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# ╔═╡ 5873ab70-c98d-11ec-2d31-c76153b8b483
using DataStructures

# ╔═╡ 1b5e48b6-9dfa-48a2-9677-e09e1694730b
mutable struct State
	x::Int64
	y::Int64
	grid::Matrix{Int64}
end

# ╔═╡ 9881f9ae-ecc3-4214-8574-e4f38844f28b
struct action
	name::String
	cost::Float64
end

# ╔═╡ 781e3a95-cf1c-43f0-8ea6-2d5ea21f6cf1
col = 5

# ╔═╡ 0fec9c82-d8c0-45a7-bf39-c7f3df2dc4c0
MoveEast = action("Move East", 2)

# ╔═╡ 5eb39507-a4b7-4d27-9ee9-a297d0215a9b
MoveWest = action("Move West", 2)

# ╔═╡ 6605cb8f-5fb2-4e90-ba56-8b1cabfba1ec
MoveUp = action("Move Up", 1)

# ╔═╡ 3fee2521-dfb6-49cd-8708-188df6581cce
MoveDown = action("Move Down", 1)

# ╔═╡ bddbf4f9-11eb-4f53-9468-e59001178913
CollectPack = action("Collect Package", col)

# ╔═╡ 62f02101-eb62-4007-ba97-516e2e658327
function spot(i,j)
	x = i
	y = j
	f = 0
	g = 0
	h = 0
end

# ╔═╡ f2f2a2fc-f89e-45e8-9f24-7b9de485c936
initState = State(1,1,[1 1 1; 1 1 1; 1 1 1; 1 1 1; 1 1 1]) 

# ╔═╡ 88232366-528d-4041-9a38-63fed3df1df3
goalGrid = [0 0 0; 0 0 0; 0 0 0; 0 0 0; 0 0 0]

# ╔═╡ 5b051e27-31d0-4934-8704-d59a63a3cd3c
function remOpenSet(openSet, current)
	for i in openSet
		if i == current
			ind = getindex(openSet,i)
			splice!(openSet, ind)
		end
	end
	return openSet
end

# ╔═╡ 7763619b-6596-4700-b796-7b6e17df2cae
transModel = []

# ╔═╡ 500ac726-2c23-4262-b199-bc5ec2d20bab
function aStar(startState, goal)
	openSet = []
	closedSet = []
	current = startState
	neigh = [] #vector to store neighbours
	push!(openSet, current)
	push!(transModel, startState)
	while !isempty(openSet)
		winner = null
		if openSet[i].grid == goal
			winner = openSet[i]
		end
		current = winner
		if current.grid == goal
			return current
		else
			openSet = remOpenSet(openSet,current)
			push!(closedSet, current)
		end
	end
end

# ╔═╡ f373954b-b72b-477b-98f9-1965f2634b81
function genState(state::State, x, y)
	newGrid = state.grid
	if newGrid[x,y] > 0 
		newGrid[x,y] = newGrid[x,y] - 1
	end
	newState = State(x,y,newGrid)
	return newState
end

# ╔═╡ 7ffc4db6-4e77-4de3-abc6-2394e19302b1
xx = genState(initState,initState.x,initState.y)

# ╔═╡ a824b1fd-94cf-41d9-b849-346bafa0e0ab


# ╔═╡ 3410d061-bf93-415a-a8bf-c6222705819c
state1 = State(1,2,[1 1 1; 1 1 1; 1 1 1; 1 1 1; 1 1 1] ) 

# ╔═╡ 36302ecf-6b7a-4366-aab1-4accb763041f
state2 = State(2,1,[1 1 1; 1 1 1; 1 1 1; 1 1 1; 1 1 1] ) 

# ╔═╡ 62a529fd-b4b4-4511-9d37-fc7b6e73bfbe
lele = genState(state2,state2.x,state2.y)

# ╔═╡ 70fb7677-17d0-4c79-b41f-61886d7142aa
state2

# ╔═╡ b51aa612-235f-41a4-9586-503dff7928e5
function calG(colCost,state::State)
	sum1 = 3
	sum2 = 0
	x = 0
	val = 0
	for i in state.grid
		if i != 0
			sum2 = colCost + sum1	
			x += 1
		end
	end
	val = sum2*x
	return val
end

# ╔═╡ 23c8da3b-4876-4e33-b257-447524b02411
lolo = calG(col,lele)

# ╔═╡ 493b46d6-0162-4156-8a56-ef00a2111a01
function calH(m,col,state::State)
	n = 0
	x = 0
	val = 0
	for i in state.grid
		if i != 0
			n = i + col + m
			x += 1
		end
	end
	val = x*n
	return val
end

# ╔═╡ 8a90abcb-48e9-4a31-87ec-8e17c0d0b7c5
function nextMove(x,y,state::State)
	v = []
	v2 = []
	x1 = x+1
	y1 = y+1
	x2 = x-1
	y2 = y-1
	actions = []
	if state.grid[x,y] != 0
		g = calG(col,state)
		tempstate1 = genState(state,x,y)
		h = calH(0,col,tempstate1)
		f = g+h
		push!(actions,(tempstate1,CollectPack,f))
	end
	try
		if state.grid[x,y1] == 0
			g = calG(col,state)
			tempstate2 = genState(state,x,y1)
			h = calH(MoveUp.cost,col,tempstate)
			f = g+h
			push!(actions,(tempState2,MoveUp,f))
		else
			g = calG(col,state)
			tempstate3 = genState(state,x,y1)
			h = calH(MoveUp.cost,col,tempstate)
			f = g+h
			push!(actions,(tempState3,MoveUp,CollectPack,f))
		end
	catch
		BoundsError()
	end
	
	try
		if state.grid[x1,y] == 0
			g = calG(col,state)
			tempstate4 = genState(state,x1,y)
			h = calH(MoveDown.cost,col,tempstate)
			f = g+h
			push!(actions,(tempState4,MoveDown,f))
		else
			g = calG(col,state)
			tempstate5 = genState(state,x1,y)
			h = calH(MoveDown.cost,col,tempstate)
			f = g+h
			push!(actions,(tempState5,MoveDown,CollectPack,f))
		end
	catch
		BoundsError()
	end
	
	try
		if state.grid[x,y1] == 0
			g = calG(col,state)
			tempstate6 = genState(state,x,y)
			h = calH(MoveEast.cost,col,tempstate)
			f = g+h
			push!(actions,(tempState6,MoveEast,f))
		else
			g = calG(col,state)
			tempstate7 = genState(state,x,y)
			h = calH(MoveEast.cost,col,tempstate)
			f = g+h
			push!(actions,(tempState7,MoveEast,CollectPack,f))
		end
	catch
		BoundsError()
	end
	
	try
		if state.grid[x,y2] == 0
			g = calG(col,state)
			tempstate8 = genState(state,x,y2)
			h = calH(MoveWest.cost,col,tempstate)
			f = g+h
			push!(actions,(tempState8,MoveWest,f))
		else
			g = calG(col,state)
			tempstate9 = genState(state,x,y2)
			h = calH(MoveWest.cost,col,tempstate)
			f = g+h
			push!(actions,(tempState9,MoveWest,CollectPack,f))
		end
	catch
		BoundsError()
	end
	pick = []
	for i in actions
		if isempty(pick)
			push!(pick, i)
		else
			if (n = last(i))<(m = last(pick[1]))
				pop!(pick)
				push!(pick, i)
			end
		end
	end
	return pick
end

# ╔═╡ 59e162bd-7887-4af2-992b-9f1ad52e9393
zz = nextMove(initState.x,initState.y,initState)

# ╔═╡ aa5752bc-8b9e-472b-af40-2a29cf2d8ab5
lili = calH(col,MoveDown.cost,state2)

# ╔═╡ 9daf7444-41c6-4d77-8898-170b5fd459dc


# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"

[compat]
DataStructures = "~0.18.11"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═5873ab70-c98d-11ec-2d31-c76153b8b483
# ╠═1b5e48b6-9dfa-48a2-9677-e09e1694730b
# ╠═9881f9ae-ecc3-4214-8574-e4f38844f28b
# ╠═781e3a95-cf1c-43f0-8ea6-2d5ea21f6cf1
# ╠═0fec9c82-d8c0-45a7-bf39-c7f3df2dc4c0
# ╠═5eb39507-a4b7-4d27-9ee9-a297d0215a9b
# ╠═6605cb8f-5fb2-4e90-ba56-8b1cabfba1ec
# ╠═3fee2521-dfb6-49cd-8708-188df6581cce
# ╠═bddbf4f9-11eb-4f53-9468-e59001178913
# ╠═62f02101-eb62-4007-ba97-516e2e658327
# ╠═f2f2a2fc-f89e-45e8-9f24-7b9de485c936
# ╠═88232366-528d-4041-9a38-63fed3df1df3
# ╠═500ac726-2c23-4262-b199-bc5ec2d20bab
# ╠═5b051e27-31d0-4934-8704-d59a63a3cd3c
# ╠═7763619b-6596-4700-b796-7b6e17df2cae
# ╠═f373954b-b72b-477b-98f9-1965f2634b81
# ╠═7ffc4db6-4e77-4de3-abc6-2394e19302b1
# ╠═8a90abcb-48e9-4a31-87ec-8e17c0d0b7c5
# ╠═59e162bd-7887-4af2-992b-9f1ad52e9393
# ╠═a824b1fd-94cf-41d9-b849-346bafa0e0ab
# ╠═3410d061-bf93-415a-a8bf-c6222705819c
# ╠═36302ecf-6b7a-4366-aab1-4accb763041f
# ╠═62a529fd-b4b4-4511-9d37-fc7b6e73bfbe
# ╠═70fb7677-17d0-4c79-b41f-61886d7142aa
# ╠═23c8da3b-4876-4e33-b257-447524b02411
# ╠═aa5752bc-8b9e-472b-af40-2a29cf2d8ab5
# ╠═b51aa612-235f-41a4-9586-503dff7928e5
# ╠═493b46d6-0162-4156-8a56-ef00a2111a01
# ╠═9daf7444-41c6-4d77-8898-170b5fd459dc
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
